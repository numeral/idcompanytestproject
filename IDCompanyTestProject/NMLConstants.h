//
//  NMLConstants.h
//  IDCompanyTestProject
//
//  Created by Vladimir Pronin on 12/14/13.
//  Copyright (c) 2013 Vladimir Pronin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NMLConstants : NSObject

FOUNDATION_EXPORT NSString const *kDownloadsFolder;


@end
