//
//  NMLDownloadManager.h
//  IDCompanyTestProject
//
//  Created by Vladimir Pronin on 12/14/13.
//  Copyright (c) 2013 Vladimir Pronin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NMLDownloadManager : NSObject

+ (NMLDownloadManager *)sharedDownloadManager;

@property (nonatomic) int numberOfConcurrectDownloads;

@property (strong, nonatomic, readonly) NSOperationQueue *downloadsQueue;

- (void)pauseDownloadOperationAtIndex:(int)index;
- (void)resumeDownloadOperationAtIndex:(int)index;
- (void)cancelDownloadOperationAtIndex:(int)index;

- (void)downloadFileAtURL:(NSURL *)fileURL toPath:(NSString *)storePath downloadProgressBlock:(void (^)(NSOperation *operation, NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead))downloadProgressBlock withSuccess:(void (^)(NSOperation *operation, id responseObject))success
                  failure:(void (^)(NSOperation *operation, NSError *error))failure;

@end
