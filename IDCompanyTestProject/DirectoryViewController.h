//
//  DirectoryViewController.h
//  IDCompanyTestProject
//
//  Created by Vladimir Pronin on 12/14/13.
//  Copyright (c) 2013 Vladimir Pronin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BrowserViewController.h"

@interface DirectoryViewController : BrowserViewController

@end
