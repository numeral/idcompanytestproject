//
//  NMLDownloadManager+Extensions.h
//  IDCompanyTestProject
//
//  Created by Vladimir Pronin on 12/14/13.
//  Copyright (c) 2013 Vladimir Pronin. All rights reserved.
//

/*
 Helper methods for displaying operations in UITableView
 */

#import "NMLDownloadManager.h"

@interface NMLDownloadManager (Extensions)

- (NSURL *)URLForDownloadOperationAtIndex:(int)index;

- (NSURL *)URLForDownloadOperation:(NSOperation *)operation;

- (int)indexOfDownloadOperation:(NSOperation *)operation;

- (BOOL)operationPausedAtIndex:(int)index;

@end
