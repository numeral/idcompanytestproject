//
//  DirectoryViewController.m
//  IDCompanyTestProject
//
//  Created by Vladimir Pronin on 12/14/13.
//  Copyright (c) 2013 Vladimir Pronin. All rights reserved.
//

#import "DirectoryViewController.h"
#import "SSZipArchive.h"

@interface DirectoryViewController () <UIActionSheetDelegate>

@end

@implementation DirectoryViewController

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(uploadFiles)];
}


- (void)uploadFiles {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Archive files", nil];
    [actionSheet showInView:self.view];
    
}


#pragma mark - UIActionSheetDelegate


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        
        [self archiveFolder];
    }
}


- (void)archiveFolder {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        
        NSString* theFileName = [[self.filesPath lastPathComponent] stringByDeletingPathExtension];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.zip", theFileName]];
        
        BOOL archived = [SSZipArchive createZipFileAtPath:path withContentsOfDirectory:self.filesPath];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (archived) {
                [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"Folder '%@' Archived", theFileName]];
            } else {
                [SVProgressHUD showErrorWithStatus:@"Archive Error"];
            }
        });
        
    });
}


- (void)dealloc {
    
    //fix to prevent crash on pop view controller
    self.tableView.editing = NO;
}

@end
