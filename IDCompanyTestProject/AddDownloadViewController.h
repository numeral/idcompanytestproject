//
//  AddDownloadViewController.h
//  IDCompanyTestProject
//
//  Created by Vladimir Pronin on 12/14/13.
//  Copyright (c) 2013 Vladimir Pronin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AddDownloadViewController;
@protocol AddDownloadViewControllerDelegate <NSObject>

- (void)addDownloadViewController:(AddDownloadViewController *)addDownloadViewController didEndEditingURL:(NSURL *)fileURL;

@end

@interface AddDownloadViewController : UIViewController

@property (weak, nonatomic) id <AddDownloadViewControllerDelegate> delegate;

@end
