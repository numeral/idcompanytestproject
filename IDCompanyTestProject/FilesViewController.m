//
//  FilesViewController.m
//  IDCompanyTestProject
//
//  Created by Vladimir Pronin on 12/14/13.
//  Copyright (c) 2013 Vladimir Pronin. All rights reserved.
//

#import "FilesViewController.h"
#import "DirectoryViewController.h"

@interface FilesViewController ()

@end

@implementation FilesViewController

- (NSString *)filesPath {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@", kDownloadsFolder]];
    return path;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *fileName = self.files[indexPath.row];
    
    DirectoryViewController *directoryViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DirectoryViewController"];
        directoryViewController.filesPath = [self.filesPath stringByAppendingPathComponent:fileName];
    [self.navigationController pushViewController:directoryViewController animated:YES];
}


@end
