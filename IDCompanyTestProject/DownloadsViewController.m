//
//  DownloadsViewController.m
//  IDCompanyTestProject
//
//  Created by Vladimir Pronin on 12/14/13.
//  Copyright (c) 2013 Vladimir Pronin. All rights reserved.
//

#import "DownloadsViewController.h"
#import "AddDownloadViewController.h"
#import "FileDownloadCell.h"
#import "SSZipArchive.h"

@interface DownloadsViewController () <UITableViewDelegate, UITableViewDataSource, AddDownloadViewControllerDelegate, FileDownloadCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableDictionary *progressDictionary;

@end

@implementation DownloadsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (NSMutableDictionary *)progressDictionary {
    
    if (!_progressDictionary) {
        
        _progressDictionary = [NSMutableDictionary dictionary];
    }
    return _progressDictionary;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.tableView registerNib:[UINib nibWithNibName:@"FileDownloadCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"FileDownloadCell"];
}


- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [NMLDownloadManager sharedDownloadManager].downloadsQueue.operations.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FileDownloadCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"FileDownloadCell"];
    cell.delegate = self;
    
    NSURL *downloadURL = [[NMLDownloadManager sharedDownloadManager] URLForDownloadOperationAtIndex:indexPath.row];
    
    cell.titleLabel.text = downloadURL.absoluteString;
    
    NSNumber *storedProgress = [self.progressDictionary objectForKey:downloadURL];
    
    if (storedProgress) {
        cell.progressView.progress = [storedProgress floatValue];
    } else {
        cell.progressView.progress = 0;
    }
    
    
    cell.isPausedStatus = [[NMLDownloadManager sharedDownloadManager] operationPausedAtIndex:indexPath.row];
    
    return cell;
}


#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"ShowAddDownloadViewController"]) {
        
        UINavigationController *addDownloadNavigationController = segue.destinationViewController;
        
        AddDownloadViewController *addDownloadViewController = [addDownloadNavigationController.viewControllers lastObject];
        
        addDownloadViewController.delegate = self;
    }
}


#pragma mark - Instance methods


- (void)addDownloadTaskWithURL:(NSURL *)fileURL {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:fileURL.lastPathComponent];
    
    [[NMLDownloadManager sharedDownloadManager] downloadFileAtURL:fileURL toPath:path downloadProgressBlock:^(NSOperation *operation, NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        
        int rowIndex = [[NMLDownloadManager sharedDownloadManager] indexOfDownloadOperation:operation];
        FileDownloadCell *cell = (FileDownloadCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:rowIndex inSection:0]];
        
        float progress = (float)totalBytesRead / (float)totalBytesExpectedToRead;
        [cell.progressView setProgress:progress animated:YES];
        
        [self.progressDictionary setObject:@(progress) forKey:[[NMLDownloadManager sharedDownloadManager] URLForDownloadOperation:operation]];
        
    } withSuccess:^(NSOperation *operation, id responseObject) {
        
        [self removeProgressForDownloadOperation:operation];

        //unzip
        NSString *fileName = [[fileURL.lastPathComponent componentsSeparatedByString:@"."] firstObject];
        [self asyncUnzipFileNamed:fileName atPath:path toDestination:[[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@", kDownloadsFolder, fileName]]];
        
    } failure:^(NSOperation *operation, NSError *error) {
        
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@", error]];
        
        [self removeProgressForDownloadOperation:operation];
        }];
}


- (void)asyncUnzipFileNamed:(NSString *)fileName atPath:(NSString *)path toDestination:(NSString *)destinationPath {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        //unzip file
        BOOL unziped = [SSZipArchive unzipFileAtPath:path toDestination:destinationPath];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (unziped) {
                
                [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"File '%@' loaded and unziped", fileName]];
                
                //remove zip file
                [[NSFileManager defaultManager] removeItemAtPath:path error:NULL];
            } else {
                
                [SVProgressHUD showErrorWithStatus:@"Empty file"];
            }
        });
        
    });
}


- (void)removeProgressForDownloadOperation:(NSOperation *)operation {
    
    [self.progressDictionary removeObjectForKey:[[NMLDownloadManager sharedDownloadManager] URLForDownloadOperation:operation]];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}


#pragma mark - AddDownloadViewControllerDelegate


- (void)addDownloadViewController:(AddDownloadViewController *)addDownloadViewController didEndEditingURL:(NSURL *)fileURL {
    
    [self addDownloadTaskWithURL:fileURL];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}


#pragma mark - FileDownloadCellDelegate


- (void)fileDownloadCell:(FileDownloadCell *)fileDownloadCell didChangeDownloadStatus:(BOOL)isPaused {
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:fileDownloadCell];
    
    if (isPaused) {
        [[NMLDownloadManager sharedDownloadManager] pauseDownloadOperationAtIndex:indexPath.row];
    } else {
        [[NMLDownloadManager sharedDownloadManager] resumeDownloadOperationAtIndex:indexPath.row];
    }
}

@end
