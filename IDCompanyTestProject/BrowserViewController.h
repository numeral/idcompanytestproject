//
//  BrowserViewController.h
//  IDCompanyTestProject
//
//  Created by Vladimir Pronin on 12/14/13.
//  Copyright (c) 2013 Vladimir Pronin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrowserViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) NSString *filesPath;
@property (strong, nonatomic) NSArray *files;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (void)refreshFiles;

@end
