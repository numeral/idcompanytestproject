//
//  FileDownloadCell.m
//  IDCompanyTestProject
//
//  Created by Vladimir Pronin on 12/14/13.
//  Copyright (c) 2013 Vladimir Pronin. All rights reserved.
//

#import "FileDownloadCell.h"
@interface FileDownloadCell ()

@property (weak, nonatomic) IBOutlet UIButton *pauseResumeButton;

@end

@implementation FileDownloadCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)setIsPausedStatus:(BOOL)isPausedStatus {
    
    _isPausedStatus = isPausedStatus;
    if (_isPausedStatus) {
        
        [self.pauseResumeButton setTitle:@"Resume" forState:UIControlStateNormal];
    } else {
        
        [self.pauseResumeButton setTitle:@"Pause" forState:UIControlStateNormal];
    }
}

- (void)awakeFromNib {
    
    self.isPausedStatus = NO;
    [self.pauseResumeButton setTitle:@"Pause" forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (IBAction)changeDownloadStatus:(id)sender {
    
    self.isPausedStatus = !self.isPausedStatus;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(fileDownloadCell:didChangeDownloadStatus:)]) {
        
        [self.delegate fileDownloadCell:self didChangeDownloadStatus:self.isPausedStatus];
    }
}

@end
