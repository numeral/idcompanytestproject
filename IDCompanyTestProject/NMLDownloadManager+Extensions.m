//
//  NMLDownloadManager+Extensions.m
//  IDCompanyTestProject
//
//  Created by Vladimir Pronin on 12/14/13.
//  Copyright (c) 2013 Vladimir Pronin. All rights reserved.
//

#import "NMLDownloadManager+Extensions.h"
#import "AFNetworking.h"


@implementation NMLDownloadManager (Extensions)

- (NSURL *)URLForDownloadOperationAtIndex:(int)index {
    
    if (index >= 0 && index < self.downloadsQueue.operations.count) {
        AFHTTPRequestOperation *operation = self.downloadsQueue.operations[index];
        
        if (operation) {
            return operation.request.URL;
        }
    }
    return nil;
}


- (NSURL *)URLForDownloadOperation:(NSOperation *)operation {
    
    AFHTTPRequestOperation *requestOperation = (AFHTTPRequestOperation *)operation;
    return requestOperation.request.URL;
}


- (int)indexOfDownloadOperation:(NSOperation *)operation {
    
    return [self.downloadsQueue.operations indexOfObject:operation];
}


- (BOOL)operationPausedAtIndex:(int)index {
    
    if (index >= 0 && index < self.downloadsQueue.operations.count) {
        AFHTTPRequestOperation *operation = self.downloadsQueue.operations[index];
        
        if (operation) {
            return operation.isPaused;
        }
    }
    return NO;
}

@end
