//
//  FileDownloadCell.h
//  IDCompanyTestProject
//
//  Created by Vladimir Pronin on 12/14/13.
//  Copyright (c) 2013 Vladimir Pronin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FileDownloadCell;
@protocol FileDownloadCellDelegate <NSObject>

- (void)fileDownloadCell:(FileDownloadCell *)fileDownloadCell didChangeDownloadStatus:(BOOL)isPaused;

@end

@interface FileDownloadCell : UITableViewCell

@property (weak, nonatomic) id <FileDownloadCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (nonatomic) BOOL isPausedStatus;


@end
