//
//  NMLDownloadManager.m
//  IDCompanyTestProject
//
//  Created by Vladimir Pronin on 12/14/13.
//  Copyright (c) 2013 Vladimir Pronin. All rights reserved.
//

#import "NMLDownloadManager.h"
#import "AFNetworking.h"


@interface NMLDownloadManager ()

@property (strong, nonatomic, readwrite) NSOperationQueue *downloadsQueue;

@end

static NMLDownloadManager *downloadManagerInstance = nil;

@implementation NMLDownloadManager


+ (NMLDownloadManager *)sharedDownloadManager {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        downloadManagerInstance = [[NMLDownloadManager alloc] init];
        // Do any other initialisation stuff here
    });
    return downloadManagerInstance;
}


- (NSOperationQueue *)downloadsQueue {
    
    if (!_downloadsQueue) {
        _downloadsQueue = [NSOperationQueue new];
    }
    return _downloadsQueue;
}


- (void)setNumberOfConcurrectDownloads:(int)numberOfConcurrectDownloads {
 
    _numberOfConcurrectDownloads = numberOfConcurrectDownloads;
    self.downloadsQueue.maxConcurrentOperationCount = _numberOfConcurrectDownloads;
}


#pragma mark - Methods


- (void)downloadFileAtURL:(NSURL *)fileURL toPath:(NSString *)storePath downloadProgressBlock:(void (^)(NSOperation *operation, NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead))downloadProgressBlock withSuccess:(void (^)(NSOperation *operation, id responseObject))success
                  failure:(void (^)(NSOperation *operation, NSError *error))failure {
    
    NSURLRequest *request = [NSURLRequest requestWithURL:fileURL];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:storePath append:NO];
    
    __weak NSOperation *weakOperation = operation;
    
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        downloadProgressBlock(weakOperation, bytesRead, totalBytesRead, totalBytesExpectedToRead);
    }];
    
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(operation, error);
    }];
    
    [self.downloadsQueue addOperation:operation];
}


- (void)pauseDownloadOperationAtIndex:(int)index {
    
    if (index >= 0 && index < self.downloadsQueue.operations.count) {
    AFHTTPRequestOperation *operation = self.downloadsQueue.operations[index];
        
        if (operation) {
            [operation pause];
        }
    }
}


- (void)resumeDownloadOperationAtIndex:(int)index {
    
    if (index >= 0 && index < self.downloadsQueue.operations.count) {
        AFHTTPRequestOperation *operation = self.downloadsQueue.operations[index];
        
        if (operation) {
            [operation resume];
        }
    }
}


- (void)cancelDownloadOperationAtIndex:(int)index {
    
    if (index >= 0 && index < self.downloadsQueue.operations.count) {
        AFHTTPRequestOperation *operation = self.downloadsQueue.operations[index];
        
        if (operation) {
            [operation cancel];
        }
    }
}


@end
